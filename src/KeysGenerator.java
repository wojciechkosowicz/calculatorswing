import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by wojtek on 06.12.15.
 */
public class KeysGenerator {
    protected JPanel buttonsPanel = null;

    public KeysGenerator(int rows, int cols) {
        buttonsPanel =  new JPanel(new GridLayout(rows, cols));
        buttonsPanel.setFocusable(false);
    }
    public JPanel buttonsPanel() { return buttonsPanel; }

    protected void generatePanelWithButtons(JTextField display) {
        for (int i = 1; i < 10; i++) {
            final Integer intValue =  new Integer(i);
            JButton button = new JButton(intValue.toString());
            buttonsPanel.add(button);
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    display.setText(display.getText() + intValue.toString());
                }
            });
        }

    }

    public void addCustomTextButton(String value, JTextField display, ActionListener action) {
        JButton button =  new JButton(value);
        buttonsPanel().add(button);
        button.addActionListener(action);
    }
}
