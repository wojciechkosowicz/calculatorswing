import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by wojtek on 04.12.15.
 */
public class MainClass {
    public static void main(String [] args) {
        System.out.println("Hiii");
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                //Create and set up the window.
                JFrame frame = new JFrame("HelloWorldSwing");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

                Container pane = frame.getContentPane();
                BasicCalcKeyGenerator keysGenerator = new BasicCalcKeyGenerator(3, 2);
                BasicCalcKeyGenerator operationsKeyGenerator = new BasicCalcKeyGenerator(3, 2);
                JTextField display = new JTextField();
                display.setEditable(false);

                display.setHorizontalAlignment(SwingConstants.RIGHT);
                keysGenerator.makeGenericKeyboardPanel(display);
                operationsKeyGenerator.makeKeyboardOperationsPanel(display);

                pane.add(display, BorderLayout.PAGE_START);
                pane.add(keysGenerator.buttonsPanel(), BorderLayout.LINE_START);
                pane.add(operationsKeyGenerator.buttonsPanel(), BorderLayout.LINE_END);
                //Display the window.
                frame.pack();
                frame.setVisible(true);


            }
        });
    }


}
