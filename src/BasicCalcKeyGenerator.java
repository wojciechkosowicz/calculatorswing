import com.sun.corba.se.spi.orb.Operation;
import jdk.nashorn.internal.runtime.OptimisticReturnFilters;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

;
public class BasicCalcKeyGenerator extends KeysGenerator {
    MathOperations mathOperations = MathOperations.getInstance();

    public BasicCalcKeyGenerator(int rows, int cols) {
        super(rows, cols);
    }

    public void makeGenericKeyboardPanel(JTextField display) {
        generatePanelWithButtons(display);
        addCustomTextButton("00", display, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                display.setText(display.getText() + "00");
            }
        });
        addCustomTextButton("0", display, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                display.setText(display.getText() + "0");
            }
        });
    }

   public void makeKeyboardOperationsPanel(JTextField display) {
        Object[][] operations = {{"+", OperationMath.ADD}, {"-", OperationMath.SUBTRACT},
                {"*", OperationMath.MULTIPLY}, {"/", OperationMath.DIVIDE}};
        for (Object[] o : operations) {
            addCustomTextButton((String) o[0], display, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    PerformOperation((OperationMath) o[1], display);
                }
            });
        }

       addCustomTextButton("SQRT", display, new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
               mathOperations.doSumOperation(OperationMath.SQRT, Double.parseDouble(display.getText()));
               display.setText(mathOperations.getResult().toString());
           }
       });
        addCustomTextButton("=", display, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mathOperations.doSumOperation(mathOperations.getOperationMath(), Double.parseDouble(display.getText()));
                display.setText(mathOperations.getResult().toString());
            }
        });

        addCustomTextButton("CE", display, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mathOperations.reset();
                display.setText("");
            }
        });
    }

    private void PerformOperation(OperationMath op, JTextField display) {
        mathOperations.doMath(op, Double.parseDouble(display.getText()));
        display.setText("");
    }
}
