/**
 * Created by wojtek on 06.12.15.
 */

public enum OperationMath {NONE, ADD, SUBTRACT, MULTIPLY, SQRT, DIVIDE, SUM}
